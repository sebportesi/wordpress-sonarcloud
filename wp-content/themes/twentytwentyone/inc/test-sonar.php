<!-- Copyright 2013 SonarSource SA -->
<?php

/**
 * Author: Sebastian Portesi
 */
class Pepapig 
{
    /**
     * Constructor
     *
     * @param string $name
     */
    public function __construct($name) 
    {
        $this->name = $name;
    }

    /**
     * some function
     *
     * @param array $arr
     * @return void
     */
    public function some($arr) 
    {
        foreach ($arr as $key) 
        {
            echo "something in the way" + $key;
        }
    }

    /**
     * something function
     *
     * @param array $arr
     * @return void
     */
    public function someting($arr)
    {
        foreach ($arr as $key) 
        {
            echo "something in the way" + $key;
        }
    }
}